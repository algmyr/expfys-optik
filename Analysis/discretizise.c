/*
 * Read 16-bit little endian mono data (in Manchester coding) from stdin
 * discretizises the signal to ones and zeroes and outputs through stdout.
 */

#include <stdio.h>
#include <stdlib.h>

#define ABS(x) (x<0 ? -x : x)

#define MAX 32767

/*
 * Buffer bits and write full shorts
 */
unsigned short twobyte;
int bitcount = 0;

void putbit(int b) {
    twobyte = (twobyte>>1) | (0x8000*b);
    if (++bitcount == 16) { fwrite(&twobyte, sizeof(short), 1, stdout); bitcount = 0; fflush(stdout); }
}

int main(int argc, char *argv[])
{
    int pulselen = 4;
    if (argc == 2) {
        pulselen = atoi(argv[1]);
    }

    // Setup variables for current and last sample
    short samp;
    short lsamp;

    // Set the trigger level for start of signal
    int triggerlevel = 1700;
    
    // Loop through multiple separate signals
    while (!feof(stdin)) {
        // Fast forward to signal
        while (fread(&samp, sizeof(short), 1, stdin) != 0 && ABS(samp) < triggerlevel);
        fprintf(stderr, "%d\n", samp);

        // Set initial state and set last sample value
        int state = samp>0 ? 1 : 0;
        lsamp=samp;

        // Walk through the data, look for sign changes and count highs and lows
        // in a row to determine what the signal is.
        int count = 1;

        while (fread(&samp, sizeof(short), 1, stdin) != 0) {
            // Continue if same sign. Otherwise, switch state and print bits.
            if ((samp>0) == (lsamp>0)) {
                ++count;
            } else {
                putbit(state);

                // End condition (very long last pulse)
                if (count > pulselen*8) break;
                
                // If long pulse, put another bit
                if (count > 3*pulselen/2) putbit(state);

                count = 1;
                state = 1 - state;
            }
            
            lsamp = samp;
        }

        // If number of bits sent isn't a multiple of our word size, something is wrong.
        if (bitcount != 0)
            fprintf(stderr, "We've missed one or more bits somewhere! Bitcount: %d\n", bitcount);
        fflush(stdout);
        bitcount = 0;
    }

    return 0;
}
