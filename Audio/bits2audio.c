/*
 * Read bits from stdin, for each bit write the relevant waveform [-32767,32767] of a
 * specified length.
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX 32767

int main(int argc, char *argv[])
{
    // Handle arguments
    int pulselen;
    if (argc != 2) {
        fprintf(stderr, "Usage:\n\t%s {samples per bit}\n", argv[0]);
        return 1;
    } else {
        pulselen = atoi(argv[1]);
    }

    int i;
    unsigned char c;

    // Current signal to be written
    short *signal = NULL;

    // Pointer to high and low signal
    short *high = malloc(pulselen*sizeof(short));
    short *low  = malloc(pulselen*sizeof(short));

    // Pointer to half high and half low signal
    short *hhigh = malloc(pulselen*sizeof(short));
    short *hlow  = malloc(pulselen*sizeof(short));

    // Set up arrays of high and low values before handling data
    for (i = 0; i < pulselen; ++i) {
        high[i]  =  MAX;
        low[i]   = -MAX;
        hhigh[i] =  MAX/2;
        hlow[i]  = -MAX/2;
    }

    // Macro to write relevant signal. First signal after transition is half high,
    // and after that it's high. This is to prevent overshoot.
#define WRITE_SIGNAL(x) signal = (x) ? ((signal==hhigh || signal==high) ? high : hhigh) \
                            : ((signal==hlow  || signal==low)  ? low  : hlow); \
                            fwrite(signal, sizeof(short), pulselen, stdout);

    // Translate bits to high and low signals
    while (fread(&c, 1, 1, stdin) != 0) {
        WRITE_SIGNAL(c&1);
        WRITE_SIGNAL(c&2);
        WRITE_SIGNAL(c&4);
        WRITE_SIGNAL(c&8);
        WRITE_SIGNAL(c&16);
        WRITE_SIGNAL(c&32);
        WRITE_SIGNAL(c&64);
        WRITE_SIGNAL(c&128);
    }
    
    // Make last pulse extra long as a makeshift eof
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);
    fwrite(signal, sizeof(short), pulselen, stdout);

    // Make sure the audio buffer will be filled, otherwise audio (apparently)
    // repeats, which is disastrous...
    short *zeros = malloc(pulselen*sizeof(short));
    zeros = calloc(sizeof(short),4096);
    fwrite(zeros, sizeof(short), 4096, stdout);

    return 0;
}

