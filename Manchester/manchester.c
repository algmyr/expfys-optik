/*
 * Given data from stdin, write Manchester coded data to stdout.
 */

#include<stdio.h>
#include<stdlib.h>

// Precalculated 16 bit sequences for 8 bit input
unsigned short mantab[256] = {43690,43689,43686,...,21849,21846,21845};

int main(int argc, char *argv[])
{
    unsigned char c;
    unsigned short s;

    while (fread(&c, 1, 1, stdin) != 0) {
        s = mantab[(int)c];
        printf("%s", (char*)&s);
    }

    return 0;
}

