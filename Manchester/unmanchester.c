/*
 * Given Manchester coded data from stdin, write original data to stdout.
 */

#include<stdio.h>
#include<stdlib.h>

// Precalculated 4 bit sequences for 8 bit sequences (of which only 16 are valid)
// We give some scrap value for invlaid inputs (undefined behavior)
#define ER 255	// Error
unsigned char imantab[256] = {\
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,15,14,ER,ER,13,12,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,11,10,ER,ER, 9, 8,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER, 7, 6,ER,ER, 5, 4,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER, 3, 2,ER,ER, 1, 0,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,
    ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER,ER
};

int main(int argc, char *argv[])
{
    unsigned short s;
    unsigned char out;

    while (fread(&s, sizeof(s), 1, stdin) != 0) {
        out = imantab[s>>8]<<4 | imantab[s&0xff];
        fwrite(&out,1,1,stdout);       
        fflush(stdout);
    }

    return 0;
}

